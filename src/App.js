import React, { Component } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Homepage from "./components/Homepage";
import Search from "./components/Search";
import CategoryFurnishings from "./components/Category/CategoryFurnishings";
import CategoryFurniture from "./components/Category/CategoryFurniture";
import CategoryHomeDecor from "./components/Category/CategoryHomeDecor";
import CategoryMatresses from "./components/Category/CategoryMatresses";
import ProductPage from "./components/ProductPage";
import SearchedProducts from "./components/SearchedProducts";
import Cart from "./components/Cart";
import Wishlist from "./components/Wishlist";
import MyOrders from "./components/MyOrders";
import Buy from "./components/Buy";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route
            path="/Category/Category_Furnishings"
            element={<CategoryFurnishings />}
          />
          <Route
            path="/Category/Category_Furnishings/products/:id"
            element={<ProductPage />}
          />
          <Route
            path="/Category/Category_Furnitures"
            element={<CategoryFurniture />}
          />
          <Route
            path="/Category/Category_Furnitures/products/:id"
            element={<ProductPage />}
          />
          <Route
            path="/Category/Category_HomeDecor"
            element={<CategoryHomeDecor />}
          />
          <Route
            path="/Category/Category_HomeDecor/products/:id"
            element={<ProductPage />}
          />
          <Route
            path="/Category/Category_Mattresses"
            element={<CategoryMatresses />}
          />
          <Route
            path="/Category/Category_Matresses/products/:id"
            element={<ProductPage />}
          />
          <Route path={`search/:searchId`} element={<Search />} />
          <Route path={`search/products/:id`} element={<SearchedProducts />} />
          <Route path="/Cart" element={<Cart />} />
          <Route path="/Buy" element={<Buy />} />
          <Route path="/Wishlist" element={<Wishlist />} />
          <Route path="/customer/myorders" element={<MyOrders />} />
        </Routes>
      </BrowserRouter>
    );
  }
}

export default App;
