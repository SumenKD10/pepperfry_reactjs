/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import validator from "validator";
import {
  ADD_TO_WISHLIST,
  DELETE_FROM_BUY,
  ORDER_THE_ITEMS,
  PURCHASE_PRODUCT,
  UPDATE_QUANTITY_CHANGE_BUY,
} from "../redux/actionTypes";
import { v4 as uuidv4 } from "uuid";

class Buy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantityChangedProduct: {},
      selectedProduct: {},
      totalPrice: 0,
      idToUpdate: "",
      orderDone: false,
      finalOrder: {},
      nameGot: "",
      numberGot: "",
      addressGot: "",
      addressFormStatus: false,
      addressToDisplay: "",
      error: {},
    };
  }

  handleQuantityChange = (id, event) => {
    this.props.updateQuantityChangeBuy(id, event.target.value);
    setTimeout(() => {
      this.toUpdatePriceFunction();
    }, 200);
  };

  cartProductClicKHandler = (id) => {
    const selectedProduct = this.props.allProducts.find((product) => {
      return product.id === id;
    });
    this.setState({
      selectedProduct: { ...selectedProduct },
    });
  };

  removeTheItem = (product) => {
    this.props.emptyBuy();
  };

  addToWishListFunction = (event, product) => {
    this.props.addToWishList(product);
  };

  toUpdatePriceFunction = () => {
    let sumOfAllPrices = 0;
    this.props.allProducts.map((eachProduct) => {
      return (sumOfAllPrices += eachProduct.price * eachProduct.quantity);
    });
    this.setState({ totalPrice: sumOfAllPrices });
  };

  orderTheItems = (event) => {
    if (this.state.addressToDisplay !== "") {
      let currentDate = new Date();
      let orderedNow = {
        orderId: uuidv4(),
        products: this.props.allProducts,
        totalPrice: this.state.totalPrice,
        time: currentDate.toString(),
      };
      this.props.orderingItems(orderedNow);
      this.setState({ orderDone: true, finalOrder: orderedNow });
      this.props.emptyBuy();
    }
  };

  handleChange = (event) => {
    let { name, value } = event.target;
    this.setState((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
    this.validatorFunction(name, value);
  };

  validatorFunction = (name, value) => {
    let { error, nameGot, numberGot, addressGot } = this.state;
    let errorMessage = "";

    if (name === "nameGot" && !validator.isAlpha(value)) {
      errorMessage = "Enter Your Name";
    } else if (
      name === "numberGot" &&
      (!validator.isMobilePhone(value) || value.length !== 10)
    ) {
      errorMessage = "Enter Valid 10-Digit Number";
    } else if (name === "addressGot" && value.length === 0) {
      errorMessage = "Enter Valid Address";
    } else {
      delete error[name];
      this.setState({
        error,
      });
    }

    if (errorMessage) {
      this.setState({
        error: {
          ...error,
          [name]: errorMessage,
        },
        addressFormStatus: false,
      });
    } else {
      if (
        nameGot.length !== 0 &&
        numberGot.length !== 0 &&
        addressGot.length !== 0
      ) {
        this.setState({ addressFormStatus: true });
      }
    }
  };

  handleAddressSave = (event) => {
    let fullAddress = `${this.state.nameGot}, ${this.state.addressGot}`;
    setTimeout(() => {
      this.setState({ addressToDisplay: fullAddress });
    }, 2 * 100);
  };

  componentDidMount() {
    this.toUpdatePriceFunction();
  }

  render() {
    return (
      <>
        <div className="d-flex justify-content-center p-4 shadow">
          <img
            src="https://ii1.pepperfry.com/assets/w22-pf-logo.svg"
            alt="no image"
          />
        </div>
        {!this.state.orderDone && (
          <>
            {this.props.allProducts.length === 0 && (
              <div className="d-flex flex-column justify-content-center mt-5">
                <h4 className="d-flex justify-content-center">
                  Your Cart is Empty
                </h4>
                <div className="d-flex justify-content-center w-100">
                  <img
                    src="https://ii2.pepperfry.com/assets/w23-empty-cart-060223.jpg"
                    alt="No Image"
                  />
                </div>
                <p className="d-flex justify-content-center">
                  What would you like to Buy ? Pick from our Best Selling
                  Categories
                </p>
                <div className="d-flex justify-content-center text-secondary">
                  <Link
                    to="/Category/Category_Furnitures"
                    className="text-secondary"
                  >
                    <h6 className="categoryEmptyCart px-1">Furniture | </h6>
                  </Link>
                  <Link
                    to="/Category/Category_HomeDecor"
                    className="text-secondary"
                  >
                    <h6 className="categoryEmptyCart px-1">Home Décor | </h6>
                  </Link>
                  <Link
                    to="/Category/Category_Furnitures"
                    className="text-secondary"
                  >
                    <h6 className="categoryEmptyCart px-1">
                      Lamps & Lighting |{" "}
                    </h6>
                  </Link>
                  <Link
                    to="/Category/Category_Mattresses"
                    className="text-secondary"
                  >
                    <h6 className="categoryEmptyCart px-1">Mattresses | </h6>
                  </Link>
                  <Link
                    to="/Category/Category_Furnishings"
                    className="text-secondary"
                  >
                    <h6 className="categoryEmptyCart px-1">Furnishings</h6>
                  </Link>
                </div>
                <Link to="/">
                  <button
                    type="button"
                    className="btn btn-lg exploreCatagoryButton w-50 mt-4 d-flex justify-content-center"
                  >
                    EXPLORE MORE CATEGORIES
                  </button>
                </Link>
              </div>
            )}
            {this.props.allProducts.length !== 0 && (
              <>
                <h4 className="d-flex justify-content-center mt-4">
                  Your Cart ({this.props.allProducts.length} items)
                </h4>
                <div className="cartDetailContainer d-flex justify-content-center gap-2">
                  <div className="allCartItemsDiv">
                    {this.props.allProducts.map((eachProduct) => {
                      return (
                        <div key={eachProduct.id}>
                          <div className="cartItemContainer d-flex mb-4">
                            <div className="p-2 d-flex flex-column justify-content-center">
                              <img
                                src={eachProduct.image}
                                alt="no-image"
                                className="cartItemImage"
                              />
                              <button
                                type="button"
                                className="btn btn-sm mt-2 p-2 btn-outline-secondary d-flex justify-content-center"
                                data-bs-toggle="modal"
                                data-bs-target="#exampleModal1"
                                onClick={() =>
                                  this.cartProductClicKHandler(eachProduct.id)
                                }
                              >
                                Move To Wishlist
                              </button>
                              <button
                                type="button"
                                className="btn btn-sm mt-2 p-2 btn-outline-secondary d-flex justify-content-center"
                                data-bs-toggle="modal"
                                data-bs-target="#exampleModal2"
                                onClick={() =>
                                  this.cartProductClicKHandler(eachProduct.id)
                                }
                              >
                                Delete
                              </button>
                            </div>
                            <div className="p-2 w-50 cartProductDetails">
                              <h6 className="mb-2">{eachProduct.title}</h6>
                              <p className="text-secondary mb-2">
                                By {eachProduct.brand}
                              </p>
                              <select
                                className="form-select myselectedProduct-3 w-25"
                                aria-label="Default select example"
                                onChange={(event) => {
                                  return this.handleQuantityChange(
                                    eachProduct.id,
                                    event
                                  );
                                }}
                                value={
                                  eachProduct.id.toString() ===
                                  this.state.idToUpdate.toString()
                                    ? this.state.quantityChangedProduct.quantity
                                    : eachProduct.quantity
                                }
                              >
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                              </select>
                              <hr className="cartRule" />
                              <div className="d-flex justify-content-sm-between">
                                <p>MRP</p>
                                <p>
                                  ₹ {eachProduct.price.toLocaleString("en-IN")}
                                </p>
                              </div>
                              <hr className="cartRule" />
                              <div className="d-flex justify-content-sm-between text-secondary">
                                <p>Delivery/Handling</p>
                                <p>FREE</p>
                              </div>
                              <hr className="cartRule" />
                              <div className="d-flex justify-content-sm-between text-secondary">
                                <p>Quantity</p>
                                <p>
                                  {eachProduct.id === this.state.idToUpdate
                                    ? this.state.quantityChangedProduct.quantity
                                    : eachProduct.quantity}
                                </p>
                              </div>
                              <hr className="cartRule" />
                              <div className="d-flex justify-content-sm-between text-secondary">
                                <p>ITEM PRICE</p>
                                <p>
                                  ₹{" "}
                                  {(
                                    eachProduct.price *
                                    (eachProduct.id === this.state.idToUpdate
                                      ? this.state.quantityChangedProduct
                                          .quantity
                                      : eachProduct.quantity)
                                  ).toLocaleString("en-IN")}
                                </p>
                              </div>
                            </div>
                          </div>
                          <div
                            className="modal fade"
                            id="exampleModal1"
                            tabIndex="-1"
                            aria-labelledby="exampleModalLabel"
                            aria-hidden="true"
                          >
                            <div className="modal-dialog modal-dialog-centered">
                              <div className="modal-content deleteFromCartModal">
                                <div className="modal-header">
                                  <h6
                                    className="modal-title"
                                    id="exampleModalLabel"
                                  >
                                    Are You Sure You want to Move This Item to
                                    Wishlist?
                                  </h6>
                                  <button
                                    type="button"
                                    className="btn-close"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  ></button>
                                </div>
                                <div className="modal-body">
                                  <div className="d-flex gap-2">
                                    <img
                                      src={this.state.selectedProduct.image}
                                      alt="no image"
                                      className="cartDeleteModalImage"
                                    />
                                    <div>
                                      <p className="text-secondary">
                                        {this.state.selectedProduct.title}
                                      </p>
                                      <p>
                                        ₹{" "}
                                        {this.state.selectedProduct.price !==
                                        undefined
                                          ? this.state.selectedProduct.price.toLocaleString(
                                              "en-IN"
                                            )
                                          : this.state.selectedProduct.price}
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div className="modal-footer">
                                  <div className="d-flex justify-content-sm-between py-5 gap-2">
                                    <button
                                      type="button"
                                      className="btn btn-lg btn-outline-secondary productButtons"
                                      data-bs-dismiss="modal"
                                    >
                                      Cancel
                                    </button>
                                    <button
                                      type="button"
                                      className="btn btn-lg productButtons addCartButton flex-1"
                                      data-bs-dismiss="modal"
                                      onClick={(event) => {
                                        this.addToWishListFunction(
                                          event,
                                          this.state.selectedProduct
                                        );
                                        this.removeTheItem();
                                      }}
                                    >
                                      Move To Wishlist
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="modal fade"
                            id="exampleModal2"
                            tabIndex="-1"
                            aria-labelledby="exampleModalLabel"
                            aria-hidden="true"
                          >
                            <div className="modal-dialog modal-dialog-centered">
                              <div className="modal-content deleteFromCartModal">
                                <div className="modal-header">
                                  <h6
                                    className="modal-title"
                                    id="exampleModalLabel"
                                  >
                                    Are You Sure You want to Remove This Item
                                    from Cart
                                  </h6>
                                  <button
                                    type="button"
                                    className="btn-close"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  ></button>
                                </div>
                                <div className="modal-body">
                                  <div className="d-flex gap-2">
                                    <img
                                      src={this.state.selectedProduct.image}
                                      alt="no image"
                                      className="cartDeleteModalImage"
                                    />
                                    <div>
                                      <p className="text-secondary">
                                        {this.state.selectedProduct.title}
                                      </p>
                                      <p>
                                        ₹{" "}
                                        {this.state.selectedProduct.price !==
                                        undefined
                                          ? this.state.selectedProduct.price.toLocaleString(
                                              "en-IN"
                                            )
                                          : this.state.selectedProduct.price}
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div className="modal-footer">
                                  <div className="d-flex justify-content-sm-between py-5 gap-2">
                                    <button
                                      type="button"
                                      className="btn btn-lg btn-outline-secondary productButtons"
                                      data-bs-dismiss="modal"
                                    >
                                      Cancel
                                    </button>
                                    <button
                                      type="button"
                                      className="btn btn-lg productButtons addCartButton flex-1"
                                      data-bs-dismiss="modal"
                                      onClick={() => {
                                        this.removeTheItem();
                                      }}
                                    >
                                      Remove
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                  <div className="p-4">
                    {this.state.addressToDisplay !== "" && (
                      <div>
                        <div className="mb-3">
                          <label
                            htmlFor="exampleFormControlTextarea1"
                            className="form-label"
                          >
                            Delivering To
                          </label>
                          <div className="p-4 cartAddressDiv">
                            {this.state.addressToDisplay === ""
                              ? "Adddress Not Given"
                              : this.state.addressToDisplay}
                          </div>
                        </div>
                      </div>
                    )}
                    <div className="p-4 cartSummaryDetail">
                      <h4 className="d-flex justify-content-center">
                        Cart Summary
                      </h4>
                      <div className="d-flex justify-content-sm-between">
                        <p>Items in Cart</p>
                        <p>{this.props.allProducts.length}</p>
                      </div>
                      <hr className="color-light" />
                      <div className="d-flex justify-content-sm-between">
                        <p>Cart Total Price</p>
                        <p>₹ {this.state.totalPrice.toLocaleString("en-IN")}</p>
                      </div>
                      <div className="d-flex justify-content-sm-between">
                        <h5>YOU PAY(Inclusive of All Taxes)</h5>
                        <h5>
                          ₹ {this.state.totalPrice.toLocaleString("en-IN")}
                        </h5>
                      </div>
                    </div>
                    {this.state.addressFormStatus === false && (
                      <button
                        type="button"
                        className="btn btn-lg mt-4 d-flex justify-content-center proceedPayButton w-100"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasRight"
                        aria-controls="offcanvasTop"
                      >
                        ADD ADDRESS DETAILS
                      </button>
                    )}
                    {this.state.addressFormStatus === true && (
                      <button
                        type="button"
                        className="btn btn-lg mt-4 d-flex justify-content-center proceedPayButton w-100"
                        onClick={(event) => {
                          return this.orderTheItems(event);
                        }}
                      >
                        PROCEED TO PAYMENT
                      </button>
                    )}
                  </div>
                </div>
              </>
            )}
          </>
        )}
        {this.state.orderDone && (
          <div className="orderSuccessPageContainer">
            <h4>
              ORDER PLACED SUCCESSFULLY
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Eo_circle_green_checkmark.svg/2048px-Eo_circle_green_checkmark.svg.png"
                alt="no Image"
                className="orderConfirmationIcon"
              />
            </h4>
            <p>
              Your Order ID Is {this.state.finalOrder.orderId} . You Will
              Recieve An Order Confirmation Email With A Link To Track Your
              Order.
            </p>
            <div className="d-flex finalOrderedCardAfterSuccess justify-content-sm-between">
              <img
                src={this.state.finalOrder.products[0].image}
                alt="No Image"
                className="orderConfirmationImage"
              />
              <div>
                <h6>
                  <img
                    src="https://freeiconshop.com/wp-content/uploads/edd/calendar-solid.png"
                    alt="noImage"
                    className="iconsConfirmationCard"
                  />
                  Delivery Details
                </h6>
                <p>{this.state.finalOrder.time.slice(4, 24)}</p>
              </div>
              <div>
                <h6>All Items:</h6>
                {this.state.finalOrder.products.map((eachProduct) => {
                  return (
                    <div key={uuidv4()}>
                      <p>{eachProduct.title}</p>
                      <p>QTY: {eachProduct.quantity}</p>
                    </div>
                  );
                })}
              </div>
              <div>
                <h6>Total Amount:</h6>
                <p>₹ {this.state.finalOrder.totalPrice}</p>
              </div>
            </div>
          </div>
        )}
        <div
          className="offcanvas offcanvas-end p-4"
          tabIndex="-1"
          id="offcanvasRight"
          aria-labelledby="offcanvasTopLabel"
        >
          <button
            type="button"
            className="btn offCanvasCloseOrderButton"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
          >
            <img
              src="https://ii1.pepperfry.com/assets/w22-close-icon-cross.svg"
              alt="No Image"
            />
          </button>
          <h5 className="my-2">Add New Address</h5>
          <hr className="emptyWishListDivDivider" />
          <div className="mb-3">
            <label htmlFor="exampleFormControlInput1" className="form-label">
              Name
            </label>
            <input
              type="text"
              className="form-control"
              name="nameGot"
              id="exampleFormControlInput1"
              placeholder="Your Name"
              onChange={(event) => {
                return this.handleChange(event);
              }}
            />
            {this.state.error.nameGot !== undefined && (
              <small className="text-danger">{this.state.error.nameGot}</small>
            )}
          </div>
          <div className="mb-3">
            <label htmlFor="exampleFormControlInput1" className="form-label">
              Mobile Number
            </label>
            <input
              type="text"
              className="form-control"
              name="numberGot"
              id="exampleFormControlInput1"
              placeholder="Your Number"
              onChange={(event) => {
                return this.handleChange(event);
              }}
            />
            {this.state.error.numberGot !== undefined && (
              <small className="text-danger">
                {this.state.error.numberGot}
              </small>
            )}
          </div>
          <div className="mb-3">
            <label htmlFor="exampleFormControlTextarea1" className="form-label">
              Address
            </label>
            <textarea
              className="form-control"
              name="addressGot"
              id="exampleFormControlTextarea1"
              rows="3"
              onChange={(event) => {
                return this.handleChange(event);
              }}
            ></textarea>
            {this.state.error.addressGot !== undefined && (
              <small className="text-danger">
                {this.state.error.addressGot}
              </small>
            )}
          </div>
          <div className="d-flex justify-content-sm-between py-5 gap-2">
            <button
              type="button"
              className="btn btn-lg btn-outline-secondary productButtons"
              data-bs-dismiss="offcanvas"
            >
              Cancel
            </button>
            {this.state.addressFormStatus === false && (
              <button
                type="button"
                className="btn btn-lg productButtons addCartButton flex-1"
              >
                Save
              </button>
            )}
            {this.state.addressFormStatus === true && (
              <button
                type="button"
                className="btn btn-lg productButtons addCartButton flex-1"
                onClick={(event) => {
                  this.handleAddressSave(event);
                }}
                data-bs-dismiss="offcanvas"
              >
                Save
              </button>
            )}
          </div>
        </div>
        <div className="d-flex justify-content-sm-between p-4 bg-light mt-5">
          <div>
            <h6>We accept</h6>
            <div className="d-flex gap-1">
              <img
                className="footerImageIcons"
                src="https://ii2.pepperfry.com/assets/w23-pf-visa.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-master-card.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-maestro.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-american-express.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii3.pepperfry.com/assets/w23-pf-rupay.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii3.pepperfry.com/assets/w23-pf-dinners-club.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii2.pepperfry.com/assets/w23-pf-wallet.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-net-banking.jpg"
                alt="no-image"
              />
            </div>
          </div>
          <div>
            <h6>100% Safe & Secure</h6>
            <div>
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com//images/icon-ck-footer-verisign-21-2x.png"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com//images/icon-ck-footer-pci-21-2x.png"
                alt="no-image"
              />
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const allProducts = state.buy.productsGot;
  const user = state.signUp.usersGot;
  return {
    allProducts,
    user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    emptyBuy: () => {
      dispatch({ type: DELETE_FROM_BUY, payload: "" });
    },
    updateQuantityChangeBuy: (id, newQuantity) => {
      dispatch({
        type: UPDATE_QUANTITY_CHANGE_BUY,
        payload: [id, newQuantity],
      });
    },
    orderingItems: (orderDetails) => {
      dispatch({ type: ORDER_THE_ITEMS, payload: orderDetails });
    },
    addToWishList: (product) => {
      dispatch({
        type: ADD_TO_WISHLIST,
        payload: product,
      });
    },
    purchaseProduct: (product) =>
      dispatch({
        type: PURCHASE_PRODUCT,
        payload: product,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Buy);
