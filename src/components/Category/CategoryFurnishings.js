/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/no-redundant-roles */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import Navbar from "../layout/Navbar";
import Footer from "../layout/Footer";
import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import { Link } from "react-router-dom";
import { ADD_TO_WISHLIST, REMOVE_FROM_WISHLIST } from "../../redux/actionTypes";

class Category_Furnishings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allDataForCategory: [],
      toFilterValues: {
        color: [],
        brand: [],
        material: [],
      },
    };
  }

  componentDidMount() {
    let allProductsForCategory = this.props.data.filter((eachData) => {
      return eachData.category === "Furnishings";
    });
    this.setState({ allDataForCategory: allProductsForCategory });
  }

  selectChange = (event) => {
    if (event.target.value !== "relevance") {
      let allProductsForCategory = this.state.allDataForCategory.sort(
        (firstData, secondData) => {
          if (firstData.price > secondData.price) {
            return event.target.value === "lowerFirst" ? 1 : -1;
          } else if (firstData.price < secondData.price) {
            return event.target.value === "lowerFirst" ? -1 : 1;
          } else {
            return 0;
          }
        }
      );
      this.setState({ allDataForCategory: allProductsForCategory });
    } else {
      let allProductsForCategory = this.props.data.filter((eachData) => {
        return eachData.category === "Furnishings";
      });
      this.setState({ allDataForCategory: allProductsForCategory });
    }
  };

  handleFilter = () => {};

  addToWishListFunction = (event, product) => {
    event.preventDefault();
    this.props.addToWishList(product);
  };

  removeFromWishListFunction = (event, product) => {
    event.preventDefault();
    this.props.removeFromWishList(product);
  };

  render() {
    return (
      <>
        <Navbar />
        <div className="d-flex justify-content-sm-between py-2">
          <div className="d-flex align-items-center filterSortContainer gap-2">
            <h6>Filter By</h6>
            <button
              className="btn"
              data-bs-toggle="offcanvas"
              href="#offcanvasExample"
              role="button"
              aria-controls="offcanvasExample"
            >
              Colour
            </button>
            <button
              className="btn"
              data-bs-toggle="offcanvas"
              href="#offcanvasExample"
              role="button"
              aria-controls="offcanvasExample"
            >
              Brand
            </button>
            <button
              className="btn"
              data-bs-toggle="offcanvas"
              href="#offcanvasExample"
              role="button"
              aria-controls="offcanvasExample"
            >
              Material
            </button>
          </div>
          <div className="d-flex align-items-center filterSortContainer">
            <h6>Sort By</h6>
            <select
              className="form-select mx-2"
              aria-label="Default select example"
              onChange={(event) => {
                this.selectChange(event);
              }}
            >
              <option value="relevance">Relevance</option>
              <option value="lowerFirst">Lowest Price First</option>
              <option value="higherFirst">Highest Price First</option>
            </select>
          </div>
        </div>
        <div className="d-flex flex-wrap gap-2 categoryContainer">
          {this.state.allDataForCategory.map((eachData) => {
            return (
              <Link
                to={`/Category/Category_Furnishings/products/${eachData.id}`}
                key={uuidv4()}
              >
                <div className="card categoryCards" key={uuidv4()}>
                  <img
                    src={eachData.image}
                    className="card-img-top"
                    alt="no image"
                  />
                  <div className="wishlistButtonContainer">
                    {!this.props.wishListIds.includes(eachData.id) && (
                      <img
                        src="https://ii3.pepperfry.com/assets/w23-vip-gallery-wishlist-unfilled.svg"
                        alt="No Image"
                        className="itemCardWishlistButton w-100"
                        onClick={(event) => {
                          return this.addToWishListFunction(event, eachData);
                        }}
                      />
                    )}
                    {this.props.wishListIds.includes(eachData.id) && (
                      <img
                        src="https://ii1.pepperfry.com/assets/w22-wishlist-active-icon.svg"
                        alt="No Image"
                        className="itemCardWishlistButton w-100"
                        onClick={(event) => {
                          return this.removeFromWishListFunction(
                            event,
                            eachData
                          );
                        }}
                      />
                    )}
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">{eachData.title}</h5>
                    <p className="card-text">{eachData.brand}</p>
                    <h2>₹{eachData.price}</h2>
                  </div>
                </div>
              </Link>
            );
          })}
        </div>
        <div
          class="offcanvas offcanvas-start"
          tabindex="-1"
          id="offcanvasExample"
          aria-labelledby="offcanvasExampleLabel"
        >
          <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasExampleLabel">
              Apply Filters
            </h5>
            <button
              type="button"
              className="btn offCanvasCloseFilterButton"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            >
              <img
                src="https://ii1.pepperfry.com/assets/w22-close-icon-cross.svg"
                alt="No Image"
              />
            </button>
          </div>
          <div class="offcanvas-body">
            <div className="d-flex flex-column">
              <div
                data-bs-toggle="collapse"
                className="d-flex align-items-start justify-content-sm-between py-4 filterCategory"
                href="#collapseExample1"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample1"
                data-parent="#accordion"
              >
                <h6>Color Swatch</h6>
                <img
                  src="https://www.iconpacks.net/icons/2/free-arrow-down-icon-3101-thumb.png"
                  className="filterIcons"
                  alt="No Image"
                />
              </div>
              <div
                class="collapse bg-light filterCategory"
                id="collapseExample1"
              >
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="orange"
                    id="flexCheckDefault"
                  />
                  <label class="form-check-label" htmlFor="flexCheckDefault">
                    Orange
                  </label>
                </div>
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="white"
                    id="flexCheckChecked"
                  />
                  <label class="form-check-label" htmlFor="flexCheckChecked">
                    White
                  </label>
                </div>
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="cream"
                    id="flexCheckDefault"
                  />
                  <label class="form-check-label" htmlFor="flexCheckDefault">
                    Cream
                  </label>
                </div>
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="blue"
                    id="flexCheckChecked"
                  />
                  <label class="form-check-label" htmlFor="flexCheckChecked">
                    Blue
                  </label>
                </div>
              </div>
              <div
                data-bs-toggle="collapse"
                className="d-flex align-items-start justify-content-sm-between py-4 filterCategory"
                href="#collapseExample2"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample2"
                data-parent="#accordion"
              >
                <h6>Brand</h6>
                <img
                  src="https://www.iconpacks.net/icons/2/free-arrow-down-icon-3101-thumb.png"
                  className="filterIcons"
                  alt="No Image"
                />
              </div>
              <div
                class="collapse bg-light filterCategory"
                id="collapseExample2"
              >
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="cortina"
                    id="flexCheckDefault"
                  />
                  <label class="form-check-label" htmlFor="flexCheckDefault">
                    Cortina
                  </label>
                </div>
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="exporthub"
                    id="flexCheckChecked"
                  />
                  <label class="form-check-label" htmlFor="flexCheckChecked">
                    Exporthub
                  </label>
                </div>
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="theConversion"
                    id="flexCheckDefault"
                  />
                  <label class="form-check-label" htmlFor="flexCheckDefault">
                    The Conversion
                  </label>
                </div>
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="story@Home"
                    id="flexCheckChecked"
                  />
                  <label class="form-check-label" htmlFor="flexCheckChecked">
                    Story@Home
                  </label>
                </div>
              </div>
              <div
                data-bs-toggle="collapse"
                className="d-flex align-items-start justify-content-sm-between py-4 filterCategory"
                href="#collapseExample3"
                role="button"
                aria-expanded="false"
                aria-controls="collapseExample3"
                data-parent="#accordion"
              >
                <h6>Material</h6>
                <img
                  src="https://www.iconpacks.net/icons/2/free-arrow-down-icon-3101-thumb.png"
                  className="filterIcons"
                  alt="No Image"
                />
              </div>
              <div
                class="collapse bg-light filterCategory"
                id="collapseExample3"
              >
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="cotton"
                    id="flexCheckDefault"
                  />
                  <label class="form-check-label" htmlFor="flexCheckDefault">
                    Cotton
                  </label>
                </div>
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value="woollen"
                    id="flexCheckChecked"
                  />
                  <label class="form-check-label" htmlFor="flexCheckChecked">
                    Woollen
                  </label>
                </div>
              </div>
              <div className="d-flex justify-content-sm-between filterButtons gap-2">
                <button className="btn filterClearButton">CLEAR ALL</button>
                <button
                  className="btn filterApplyButton"
                  onClick={this.handleFilter()}
                >
                  APPLY
                </button>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const data = state.allProductsData.data;
  const wishListIds = state.wishlist.productsGot.map((eachData) => {
    return eachData.id;
  });
  return {
    data,
    wishListIds,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToWishList: (product) =>
      dispatch({
        type: ADD_TO_WISHLIST,
        payload: product,
      }),
    removeFromWishList: (product) =>
      dispatch({
        type: REMOVE_FROM_WISHLIST,
        payload: product,
      }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Category_Furnishings);
