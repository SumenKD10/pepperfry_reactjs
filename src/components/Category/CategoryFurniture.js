/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import Navbar from "../layout/Navbar";
import Footer from "../layout/Footer";
import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import { Link } from "react-router-dom";
import { ADD_TO_WISHLIST, REMOVE_FROM_WISHLIST } from "../../redux/actionTypes";

class Category_Furniture extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allDataForCategory: [],
    };
  }

  componentDidMount() {
    let allProductsForCategory = this.props.data.filter((eachData) => {
      return eachData.category === "Furniture";
    });
    this.setState({ allDataForCategory: allProductsForCategory });
  }

  addToWishListFunction = (event, product) => {
    console.log("I Clicked");
    console.log("EVENT", event);
    event.preventDefault();
    this.props.addToWishList(product);
  };

  removeFromWishListFunction = (event, product) => {
    event.preventDefault();
    this.props.removeFromWishList(product);
  };

  selectChange = (event) => {
    if (event.target.value !== "relevance") {
      let allProductsForCategory = this.state.allDataForCategory.sort(
        (firstData, secondData) => {
          if (firstData.price > secondData.price) {
            return event.target.value === "lowerFirst" ? 1 : -1;
          } else if (firstData.price < secondData.price) {
            return event.target.value === "lowerFirst" ? -1 : 1;
          } else {
            return 0;
          }
        }
      );
      this.setState({ allDataForCategory: allProductsForCategory });
    } else {
      let allProductsForCategory = this.props.data.filter((eachData) => {
        return eachData.category === "Furniture";
      });
      this.setState({ allDataForCategory: allProductsForCategory });
    }
  };

  render() {
    return (
      <>
        <Navbar />
        <div className="d-flex justify-content-sm-between px-5 py-2">
          <div className="d-flex align-items-center px-5 filterSortContainer gap-2">
            <h6>Filter By</h6>
            <button className="btn">Colour</button>
            <button className="btn">Brand</button>
            <button className="btn">Material</button>
          </div>
          <div className="d-flex align-items-center px-5 filterSortContainer">
            <h6>Sort By</h6>
            <select
              className="form-select mx-2"
              aria-label="Default select example"
              onChange={(event) => {
                this.selectChange(event);
              }}
            >
              <option value="relevance">Relevance</option>
              <option value="lowerFirst">Lowest Price First</option>
              <option value="higherFirst">Highest Price First</option>
            </select>
          </div>
        </div>
        <div className="d-flex flex-wrap gap-2 categoryContainer">
          {this.state.allDataForCategory.map((eachData) => {
            return (
              <Link
                to={`/Category/Category_Furnitures/products/${eachData.id}`}
                key={uuidv4()}
              >
                <div className="card categoryCards" key={uuidv4()}>
                  <img
                    src={eachData.image}
                    className="card-img-top"
                    alt="no image"
                  />
                  <div className="wishlistButtonContainer">
                    {!this.props.wishListIds.includes(eachData.id) && (
                      <img
                        src="https://ii3.pepperfry.com/assets/w23-vip-gallery-wishlist-unfilled.svg"
                        alt="No Image"
                        className="itemCardWishlistButton w-100"
                        onClick={(event) => {
                          return this.addToWishListFunction(event, eachData);
                        }}
                      />
                    )}
                    {this.props.wishListIds.includes(eachData.id) && (
                      <img
                        src="https://ii1.pepperfry.com/assets/w22-wishlist-active-icon.svg"
                        alt="No Image"
                        className="itemCardWishlistButton w-100"
                        onClick={(event) => {
                          return this.removeFromWishListFunction(
                            event,
                            eachData
                          );
                        }}
                      />
                    )}
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">{eachData.title}</h5>
                    <p className="card-text">{eachData.brand}</p>
                    <h2>₹{eachData.price}</h2>
                  </div>
                </div>
              </Link>
            );
          })}
        </div>
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const data = state.allProductsData.data;
  const wishListIds = state.wishlist.productsGot.map((eachData) => {
    return eachData.id;
  });
  return {
    data,
    wishListIds,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToWishList: (product) =>
      dispatch({
        type: ADD_TO_WISHLIST,
        payload: product,
      }),
    removeFromWishList: (product) =>
      dispatch({
        type: REMOVE_FROM_WISHLIST,
        payload: product,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Category_Furniture);

// export default Category;
