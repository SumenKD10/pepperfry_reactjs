/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";

class FeaturePepperFry extends Component {
  render() {
    return (
      <div className="d-flex gap-5 justify-content-center mt-5">
        <div>
          <img
            src="https://ii1.pepperfry.com/assets/w22-delivery-usp-icon.svg"
            alt="No Image"
          />
          <p className="d-inline">10 Million Happy Deliveries</p>
        </div>
        <div>
          <img
            src="https://ii1.pepperfry.com/assets/w22-studio-usp-icon.svg"
            alt="No Image"
          />
          <p className="d-inline">200+ Studios Across 100+ Cities</p>
        </div>
        <div>
          <img
            src="https://ii1.pepperfry.com/assets/w22-return-usp-icon.svg"
            alt="No Image"
          />
          <p className="d-inline">7 Day Easy Return Policy</p>
        </div>
      </div>
    );
  }
}

export default FeaturePepperFry;
