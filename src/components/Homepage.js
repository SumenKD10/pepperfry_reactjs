/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import FeaturePepperFry from "./FeaturePepperFry";
import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
import { v4 as uuidv4 } from "uuid";
import { connect } from "react-redux";

class Homepage extends Component {
  render() {
    return (
      <>
        <Navbar />
        <div className="p-5">
          <img
            className="w-100"
            src="https://ii2.pepperfry.com/assets/5a384994-8ce8-4934-9007-2e17ba872de7.jpg"
            alt="no image available"
          />
          <div className="mt-4 d-flex justify-content-sm-between">
            <img
              className="featureFlex"
              src="https://ii3.pepperfry.com/assets/ec489047-1287-4323-b993-96e17c78fe50.jpg"
              alt="no image available"
            />
            <img
              className="featureFlex"
              src="https://ii2.pepperfry.com/assets/80a5932c-31e7-4cf4-87a2-976d7594696a.jpg"
              alt="no image available"
            />
            <img
              className="featureFlex"
              src="https://ii3.pepperfry.com/assets/0592fb32-87a2-4a46-9aa9-e513ba0e6e5c.jpg"
              alt="no image available"
            />
          </div>
          <h4 className="d-flex justify-content-center mt-5 homepageTitles">
            Shop What the Fry Deals
          </h4>
          <div className="d-flex mt-4 justify-content-sm-between">
            {this.props.allHomepageData.shopWhatTheFryDeals.map((eachData) => {
              return (
                <div className="dealsContainer" key={uuidv4()}>
                  <img
                    src={eachData.image}
                    alt="no-image-available"
                    className="w-100"
                  />
                  <h6>{eachData.name.slice(0, 32)}...</h6>
                  <p>₹{eachData.price}</p>
                </div>
              );
            })}
          </div>
          <h4 className="d-flex justify-content-center mt-5 homepageTitles">
            Explore Most Wanted
          </h4>
          <div className="d-flex justify-content-sm-between flex-wrap">
            {this.props.allHomepageData.exploreMostWanted.map((eachData) => {
              return (
                <div className="mostWantedContainer" key={uuidv4()}>
                  <img
                    src={eachData.image}
                    alt="no-image-available"
                    className="w-100"
                  />
                  <h6>{eachData.title_small}</h6>
                  <p>{eachData.title_large}</p>
                </div>
              );
            })}
          </div>
          <h4 className="d-flex justify-content-center mt-5 homepageTitles">
            Share Your Love
          </h4>
          <div
            id="carouselExampleControls"
            className="carousel slide"
            data-bs-ride="carousel"
            data-ride="carousel"
            data-interval="4000"
          >
            <div className="carousel-inner" role="list">
              <div className="carousel-item active">
                <div className="row">
                  {this.props.allHomepageData.shareLove
                    .slice(5, 10)
                    .map((eachData) => {
                      return (
                        <div className="shareLoveDiv" key={uuidv4()}>
                          <div className="card mx-2">
                            <img
                              className="card-img-top w-100 shareLoveImage"
                              src={eachData.image}
                              alt="Card image cap"
                            />
                            <div className="card-body bg-grey">
                              <h5 className="card-title d-flex justify-content-center">
                                {eachData.userName}
                              </h5>
                              <p className="card-text d-flex justify-content-center">
                                @{eachData.userName}
                              </p>
                              <img
                                src="https://cdn-icons-png.flaticon.com/512/174/174855.png"
                                alt="No Image"
                                className="igIcon"
                              />
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </div>
              <div className="carousel-item">
                <div className="row">
                  {this.props.allHomepageData.shareLove
                    .slice(5, 10)
                    .map((eachData) => {
                      return (
                        <div className="shareLoveDiv" key={uuidv4()}>
                          <div className="card mx-2">
                            <img
                              className="card-img-top w-100 shareLoveImage"
                              src={eachData.image}
                              alt="Card image cap"
                            />
                            <div className="card-body bg-grey">
                              <h5 className="card-title d-flex justify-content-center">
                                {eachData.userName}
                              </h5>
                              <p className="card-text d-flex justify-content-center">
                                @{eachData.userName}
                              </p>
                              <img
                                src="https://cdn-icons-png.flaticon.com/512/174/174855.png"
                                alt="No Image"
                                className="igIcon"
                              />
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </div>
            </div>
            <button
              className="carousel-control-prev"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Previous</span>
            </button>
            <button
              className="carousel-control-next"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Next</span>
            </button>
          </div>
          <h4 className="d-flex justify-content-center mt-5 homepageTitles">
            Discover Our Newest Arrivals
          </h4>
          <div className="d-flex justify-content-sm-between gap-4">
            {this.props.allHomepageData.discoverNewestArrivals.map(
              (eachData) => {
                return (
                  <div className="w-25" key={uuidv4()}>
                    <img
                      src={eachData.image}
                      alt="no-image-available"
                      className="w-100"
                    />
                    <h6>{eachData.title}</h6>
                    <p>{eachData.startingPrice}</p>
                  </div>
                );
              }
            )}
          </div>
          <h4 className="d-flex justify-content-center mt-5 homepageTitles">
            Follow Home Interior Trends
          </h4>
          <div className="d-flex justify-content-sm-between flex-wrap">
            {this.props.allHomepageData.followHomeTrends.map((eachData) => {
              return (
                <div className="interiorTrends" key={uuidv4()}>
                  <img
                    src={eachData.image}
                    alt="no-image-available"
                    className="w-100"
                  />
                  <h6>{eachData.title}</h6>
                  <p>{eachData.startingPrice}</p>
                </div>
              );
            })}
          </div>
          <h4 className="d-flex justify-content-center mt-5 homepageTitles">
            Check Out These Collections
          </h4>
          <div className="d-flex justify-content-sm-between">
            {this.props.allHomepageData.checkTheseCollections.map(
              (eachData) => {
                return (
                  <div className="collectionContainer" key={uuidv4()}>
                    <img
                      src={eachData.image}
                      alt="no-image-available"
                      className="w-100"
                    />
                    <h6>{eachData.title}</h6>
                    <p>
                      <span>&#8377;</span>
                      {eachData.startingPrice}
                    </p>
                  </div>
                );
              }
            )}
          </div>
          <hr />
          <FeaturePepperFry />
        </div>
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  let allHomepageData = state.allProductsData.allHomepageData;
  return {
    allHomepageData,
  };
};

export default connect(mapStateToProps)(Homepage);
