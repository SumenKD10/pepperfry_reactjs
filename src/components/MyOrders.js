/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";
import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";

class MyOrders extends Component {
  render() {
    return (
      <>
        <Navbar />
        <div className="d-flex wishListContainer gap-4">
          <div className="d-flex leftSectionWishList gap-4 align-items-center w-25">
            <img
              src="https://ii1.pepperfry.com/assets/w23-my-order-active.svg"
              className="wishlistsectionIcon"
            />
            <div className="w-100">
              <h6>My Orders →</h6>
            </div>
          </div>
          <div className="w-75">
            <div>
              <h5>All Orders({this.props.allOrders.length})</h5>
            </div>
            {this.props.allOrders.length === 0 && (
              <>
                <p className="d-flex justify-content-center">Oh-No!</p>
                <p className="d-flex justify-content-center">
                  Nothing To See Here
                </p>
                <div className="d-flex justify-content-center">
                  <img
                    src="https://ii1.pepperfry.com/assets/w23-myo-no-data.png"
                    alt="No-Image"
                    className="w-50"
                  />
                </div>
                <div className="d-flex justify-content-center">
                  <button
                    type="button"
                    className="btn signUpContinueButton mt-4 text-white w-50 mb-4 p-2"
                  >
                    CONTINUE SHOPPING
                  </button>
                </div>
              </>
            )}
            {this.props.allOrders.length !== 0 && (
              <>
                {this.props.allOrders.map((eachOrder) => {
                  return (
                    <div key={uuidv4()}>
                      <div className="orderCards d-flex p-5 mb-2 justify-content-sm-between">
                        <div>
                          <h6>Order ID:</h6>
                          <h6>{eachOrder.orderId}</h6>
                        </div>
                        <div>
                          <p>Total Items:</p>
                          <p>{eachOrder.products.length}</p>
                        </div>
                        <div>
                          <p>{eachOrder.time.slice(0, 24)}</p>
                        </div>
                        <div className="w-25">
                          <h4>Total Paid</h4>
                          <h4>
                            ₹ {eachOrder.totalPrice.toLocaleString("en-IN")}
                          </h4>
                        </div>
                        <div>
                          <button
                            className="btn proceedPayButton"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#collapseExample"
                            aria-expanded="false"
                            aria-controls="collapseExample"
                          >
                            Details
                          </button>
                        </div>
                      </div>
                      <div className="collapse mb-2" id="collapseExample">
                        <div className="card card-body">
                          {eachOrder.products.map((eachProduct) => {
                            return (
                              <div
                                className="d-flex orderedProductsCard justify-content-sm-between gap-2"
                                key={uuidv4()}
                              >
                                <div>
                                  <img src={eachProduct.image} alt="No Image" />
                                </div>
                                <div className="w-50">
                                  <h6>{eachProduct.title}</h6>
                                </div>
                                <div>
                                  <h6>{eachProduct.quantity}</h6>
                                </div>
                                <div>
                                  <h6>
                                    ₹{" "}
                                    {(
                                      eachProduct.price * eachProduct.quantity
                                    ).toLocaleString("en-IN")}
                                  </h6>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  );
                })}
              </>
            )}
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  let allOrders = state.orderedDetails.ordersGot;
  return {
    allOrders,
  };
};

export default connect(mapStateToProps)(MyOrders);
