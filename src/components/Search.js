/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import { Link } from "react-router-dom";
import { ADD_TO_WISHLIST, REMOVE_FROM_WISHLIST } from "../redux/actionTypes";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allDataForSearch: [],
    };
  }

  componentDidMount() {
    const searchValueGot = window.location.href.split("/").slice(4)[0];
    let allProductsForSearch = this.props.data.filter((eachData) => {
      return (
        eachData.title.includes(searchValueGot) ||
        eachData.brand.includes(searchValueGot) ||
        eachData.category.includes(searchValueGot)
      );
    });
    this.setState({ allDataForSearch: allProductsForSearch });
  }

  addToWishListFunction = (event, product) => {
    event.preventDefault();
    this.props.addToWishList(product);
  };

  removeFromWishListFunction = (event, product) => {
    event.preventDefault();
    this.props.removeFromWishList(product);
  };

  render() {
    return (
      <div>
        <Navbar />
        <div className="d-flex flex-wrap gap-2 SearchContainer p-4">
          {this.state.allDataForSearch.map((eachData) => {
            return (
              <Link to={`/search/products/${eachData.id}`} key={uuidv4()}>
                <div className="card categoryCards" key={uuidv4()}>
                  <img
                    src={eachData.image}
                    className="card-img-top"
                    alt="no image"
                  />
                  <div className="wishlistButtonContainer">
                    {!this.props.wishListIds.includes(eachData.id) && (
                      <img
                        src="https://ii3.pepperfry.com/assets/w23-vip-gallery-wishlist-unfilled.svg"
                        alt="No Image"
                        className="itemCardWishlistButton w-100"
                        onClick={(event) => {
                          return this.addToWishListFunction(event, eachData);
                        }}
                      />
                    )}
                    {this.props.wishListIds.includes(eachData.id) && (
                      <img
                        src="https://ii1.pepperfry.com/assets/w22-wishlist-active-icon.svg"
                        alt="No Image"
                        className="itemCardWishlistButton w-100"
                        onClick={(event) => {
                          return this.removeFromWishListFunction(
                            event,
                            eachData
                          );
                        }}
                      />
                    )}
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">{eachData.title}</h5>
                    <p className="card-text">{eachData.brand}</p>
                    <h2>₹{eachData.price}</h2>
                  </div>
                </div>
              </Link>
            );
          })}
        </div>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const data = state.allProductsData.data;
  const wishListIds = state.wishlist.productsGot.map((eachData) => {
    return eachData.id;
  });
  return {
    data,
    wishListIds,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToWishList: (product) =>
      dispatch({
        type: ADD_TO_WISHLIST,
        payload: product,
      }),
    removeFromWishList: (product) =>
      dispatch({
        type: REMOVE_FROM_WISHLIST,
        payload: product,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
