/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";
import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
import { v4 as uuidv4 } from "uuid";
import {
  ADD_TO_CART,
  ADD_TO_WISHLIST,
  PURCHASE_PRODUCT,
  REMOVE_FROM_WISHLIST,
} from "../redux/actionTypes";
import { Link } from "react-router-dom";

export class SearchedProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addStatus: false,
      productDetails: {},
      totalPrice: 0,
    };
  }

  addToCart = (productGot) => {
    this.setState({ addStatus: true });
    this.props.addCartItem(productGot);
  };

  addToWishListFunction = (event, product) => {
    this.props.addToWishList(product);
  };

  removeFromWishListFunction = (event, product) => {
    this.props.removeFromWishList(product);
  };

  purchaseProductFunction = (productGot) => {
    this.props.purchaseProduct(productGot);
  };

  handleChange = (event) => {
    this.setState({
      productDetails: {
        ...this.state.productDetails,
        quantity: Number(event.target.value),
      },
      totalPrice: this.state.productDetails.price * event.target.value,
    });
  };

  componentDidMount() {
    let productId = window.location.href.split("/").slice(5)[0];
    let productFound = this.props.data.find((eachData) => {
      return eachData.id.toString() === productId.toString();
    });
    this.setState({
      totalPrice: productFound.price,
      productDetails: {
        ...productFound,
        quantity: 1,
      },
    });
  }

  render() {
    const imageArrayMade = Array.from({ length: 5 }, () => {
      return 1;
    });
    return (
      <>
        <Navbar />
        <div className="d-flex productDetailContainer gap-4">
          <div className="d-flex flex-column gap-2 productOtherImages">
            {imageArrayMade.map((eachData) => {
              return (
                <div className="w-100" key={uuidv4()}>
                  <img
                    src={this.state.productDetails.image}
                    alt="none"
                    className="w-100"
                  />
                </div>
              );
            })}
          </div>
          <div>
            <img
              src={this.state.productDetails.image}
              alt="No Image"
              className="productImage"
            />
          </div>
          <div className="productDetails">
            <h6 className="text-secondary">
              {this.state.productDetails.title},
              <span className="productBrandName">
                {" "}
                By {this.state.productDetails.brand}
              </span>
            </h6>
            <hr className="color-secondary" />
            <div>
              <div className="d-flex gap-5">
                <h5>Today's Deal</h5>
                <div>
                  <h4>₹{this.state.productDetails.price}</h4>
                  <p>No Cost EMI from ₹14,583/mo</p>
                  <p className="text-success">
                    Apply Coupon MAYDAY to Avail Today’s Deal
                  </p>
                </div>
              </div>
            </div>
            <hr className="color-secondary" />
            <div className="d-flex align-items-center justify-content-sm-between">
              <div>
                <select
                  className="form-select"
                  aria-label="Default select example"
                  onChange={(event) => {
                    return this.handleChange(event);
                  }}
                >
                  <option value="1" defaultValue>
                    1
                  </option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
              </div>
              <div className="text-bold">
                Total Price
                <span className="totalPrice">
                  ₹ {this.state.totalPrice.toLocaleString("en-IN")}
                </span>
              </div>
            </div>
            <div className="d-flex justify-content-sm-between py-5 gap-2">
              {this.state.addStatus === false && (
                <button
                  type="button"
                  className="btn btn-lg productButtons addCartButton"
                  onClick={() => {
                    if (this.state.addStatus === false) {
                      this.addToCart(this.state.productDetails);
                    } else {
                      return;
                    }
                  }}
                >
                  ADD TO CART
                </button>
              )}
              {this.state.addStatus === true && (
                <Link to="/Cart">
                  <button
                    type="button"
                    className="btn btn-lg productButtons addCartButton"
                  >
                    GO TO CART
                  </button>
                </Link>
              )}
              <Link to="/Buy">
                <button
                  type="button"
                  className="btn btn-lg productButtons buyButton"
                  onClick={(event) => {
                    return this.purchaseProductFunction(
                      this.state.productDetails
                    );
                  }}
                >
                  BUY NOW
                </button>
              </Link>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const data = state.allProductsData.data;
  const wishListIds = state.wishlist.productsGot.map((eachData) => {
    return eachData.id;
  });
  return {
    data,
    wishListIds,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addCartItem: (product) =>
      dispatch({
        type: ADD_TO_CART,
        payload: product,
      }),
    addToWishList: (product) =>
      dispatch({
        type: ADD_TO_WISHLIST,
        payload: product,
      }),
    removeFromWishList: (product) =>
      dispatch({
        type: REMOVE_FROM_WISHLIST,
        payload: product,
      }),
    purchaseProduct: (product) =>
      dispatch({
        type: PURCHASE_PRODUCT,
        payload: product,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchedProducts);
