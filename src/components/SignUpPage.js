/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import FeaturePepperFry from "./FeaturePepperFry";
import validator from "validator";
import { SIGNUPLOGIN } from "../redux/actionTypes";
import { connect } from "react-redux";

class SignUpPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numberEmail: "",
      personName: "",
      errorStatus: false,
      error: {},
      signUpSuccess: false,
    };
  }

  handleChange = (event) => {
    let { name, value } = event.target;
    this.setState({
      [name]: value,
    });
    if (value.length === 0) {
      this.setState({ errorStatus: false });
    } else {
      this.validatorFunction(name, value);
    }
  };

  validatorFunction = (name, value) => {
    let { error } = this.state;
    let errorMessage = "";
    let digitCheckRegex = /^\d/;

    if (name === "numberEmail") {
      if (digitCheckRegex.test(value.split("")[0])) {
        if (!validator.isMobilePhone(value) || value.length !== 10) {
          errorMessage = "Invalid Mobile Number";
        } else {
          delete error[name];
          this.setState({
            error,
          });
        }
      } else {
        if (!validator.isEmail(value)) {
          errorMessage = "Invalid Email Id";
        } else {
          delete error[name];
          this.setState({
            error,
          });
        }
      }
    } else {
      delete error[name];
      this.setState({
        error,
        errorStatus: false,
      });
    }

    if (errorMessage) {
      this.setState({
        error: {
          ...error,
          [name]: errorMessage,
        },
      });
    } else {
      return;
    }
  };

  handleContinueFirst = (event) => {
    if (Object.values(this.state.error).length !== 0) {
      this.setState({ errorStatus: true });
    } else {
      this.setState({ errorStatus: false });
      this.setState({ signUpSuccess: true });
    }
  };

  handleContinueSecond = (event) => {
    if (this.state.personName.length === 0) {
      this.setState({ errorStatus: true });
    } else {
      this.setState({ errorStatus: false });
      this.props.insertUser(this.state.personName);
    }
  };

  render() {
    let { errorStatus } = this.state;
    return (
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content px-4">
            <div className="d-flex justify-content-end pt-4">
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body"></div>
            <img
              src="https://ii1.pepperfry.com/assets/c2839d3d-bd60-4f07-81ac-5ba776a87d63.jpg"
              alt="No Image"
              className="signUpImage"
            />

            {!this.state.signUpSuccess && (
              <>
                <h4 className="d-flex justify-content-center signUpHeading mb-2 mt-4">
                  Sign Up Or Log In
                </h4>
                <input
                  type="text"
                  className="form p-2"
                  name="numberEmail"
                  placeholder="Enter Mobile No. or Email Id"
                  aria-label="Recipient's username"
                  aria-describedby="button-addon2"
                  onChange={(event) => {
                    return this.handleChange(event);
                  }}
                />
                {errorStatus && (
                  <small className="text-danger">
                    {this.state.error.numberEmail}
                  </small>
                )}

                <button
                  type="submit"
                  className="btn signUpContinueButton mt-4 text-white"
                  onClick={(event) => {
                    return this.handleContinueFirst(event);
                  }}
                >
                  CONTINUE
                </button>
                <p className="d-flex justify-content-center text-secondary">
                  By continuing, you agree to our Terms & Conditions
                </p>
                <h6 className="d-flex justify-content-center text-secondary">
                  Or
                </h6>
                <p className="d-flex justify-content-center text-secondary">
                  Continue with
                </p>
                <button className="btn border-secondary m-2">
                  <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png"
                    alt="No Image"
                    className="mx-2 googleSignUpImage"
                  />
                  Sign in with Google
                </button>
                <button className="btn border-secondary m-2">
                  <img
                    src="https://ii1.pepperfry.com/assets/w22-facebook-icon-24.svg"
                    alt="no-image"
                  />
                  Facebook
                </button>
              </>
            )}
            {this.state.signUpSuccess && (
              <>
                <h4 className="d-flex justify-content-center signUpHeading mb-2 mt-4">
                  Complete Sign Up
                </h4>
                <input
                  type="text"
                  className="form p-2"
                  name="personName"
                  placeholder="Enter Name"
                  aria-label="Recipient's username"
                  aria-describedby="button-addon2"
                  onChange={(event) => {
                    return this.handleChange(event);
                  }}
                />
                <button
                  type="submit"
                  className="btn signUpContinueButton mt-4 text-white"
                  onClick={(event) => {
                    return this.handleContinueSecond(event);
                  }}
                  data-bs-dismiss="modal"
                >
                  CONTINUE
                </button>
              </>
            )}
            <div className="p-4 signUpAllFeature">
              <FeaturePepperFry />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    insertUser: (userName) =>
      dispatch({
        type: SIGNUPLOGIN,
        payload: userName,
      }),
  };
};

export default connect(null, mapDispatchToProps)(SignUpPage);
