/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import Navbar from "./layout/Navbar";
import Footer from "./layout/Footer";
import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import { Link } from "react-router-dom";
import { REMOVE_FROM_WISHLIST } from "../redux/actionTypes";

class Wishlist extends Component {
  removeFromWishListFunction = (event, product) => {
    event.preventDefault();
    this.props.removeFromWishList(product);
  };

  render() {
    return (
      <>
        <Navbar />
        <div className="d-flex wishListContainer gap-4">
          <div className="d-flex leftSectionWishList gap-4 align-items-center">
            <img
              src="https://ii1.pepperfry.com/assets/w23-wishlist-active.svg"
              className="wishlistsectionIcon"
            />
            <div className="w-100">
              <h6>My Wishlist →</h6>
              <p>Your Favourites All In One Place</p>
            </div>
          </div>
          <div className="allProductsWishListDiv">
            <img
              src="https://ii3.pepperfry.com/assets/22ee44d5-b120-44ee-901f-ca8e0f040a58.jpg"
              alt="No Image"
              className="cartMainImage mx-2"
            />
            {this.props.allProducts.length === 0 && (
              <div className="emptyWishlistContainer">
                <h4 className="d-flex justify-content-center mt-4">OMG!</h4>
                <h4 className="d-flex justify-content-center">
                  Your wishlist is empty
                </h4>
                <div className="d-flex justify-content-center">
                  <img
                    src="https://ii3.pepperfry.com/assets/w23-empty-wishlist.png"
                    alt="no-image"
                  />
                </div>
                <div className="d-flex justify-content-center">
                  <button
                    type="button"
                    className="btn signUpContinueButton mt-4 text-white w-50 mb-4 p-2"
                  >
                    START WISHLISTING
                  </button>
                </div>
              </div>
            )}
            {this.props.allProducts.length !== 0 && (
              <div className="d-flex flex-wrap m-4">
                {this.props.allProducts.map((eachData) => {
                  return (
                    <Link to={`/search/products/${eachData.id}`} key={uuidv4()}>
                      <div className="card wishlistCards" key={uuidv4()}>
                        <img
                          src={eachData.image}
                          className="card-img-top"
                          alt="no image"
                        />
                        <div className="wishlistButtonContainer">
                          <img
                            src="https://ii1.pepperfry.com/assets/w22-wishlist-active-icon.svg"
                            alt="No Image"
                            className="itemCardWishlistButton w-100"
                            onClick={(event) => {
                              return this.removeFromWishListFunction(
                                event,
                                eachData
                              );
                            }}
                          />
                        </div>
                        <div className="card-body">
                          <h5 className="card-title">{eachData.title}</h5>
                          <p className="card-text">{eachData.brand}</p>
                          <h2>₹{eachData.price}</h2>
                        </div>
                      </div>
                    </Link>
                  );
                })}
              </div>
            )}
            <hr className="borderAfterWishlist" />
            <p className="d-flex justify-content-center text-secondary">
              List End Here
            </p>
            <hr className="emptyWishListDivDivider mt-4" />
            <div className="d-flex justify-content-center lastSectionEmptyWishlist">
              <button type="button" className="btn addCartButton mb-2 w-25 p-2">
                EXPLORE MORE PRODUCTS
              </button>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const allProducts = state.wishlist.productsGot;
  return {
    allProducts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeFromWishList: (product) =>
      dispatch({
        type: REMOVE_FROM_WISHLIST,
        payload: product,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Wishlist);
