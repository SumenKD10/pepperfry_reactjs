/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <div className="p-5 mt-4 footerContent">
        <div className="d-flex justify-content-sm-between px-5 mt-4">
          <div>
            <h6>Corporate</h6>
            <ul className="list-unstyled text-secondary">
              <li>About Us</li>
              <li>Corporate Governance</li>
              <li>Pepperfry in the News</li>
              <li>Careers</li>
            </ul>
          </div>
          <div>
            <h6>Useful Links</h6>
            <ul className="list-unstyled text-secondary">
              <li>Explore Gift Cards</li>
              <li>Buy in Bulk</li>
              <li>Discover Our Brands</li>
              <li>Check Out Bonhomie, Our Blog</li>
              <li>Find a Studio</li>
            </ul>
          </div>
          <div>
            <h6>Partner With Us</h6>
            <ul className="list-unstyled text-secondary">
              <li>Sell on Pepperfry</li>
              <li>Become a Franchisee</li>
              <li>Become Our Channel Partner</li>
              <li>Become Our Pep Homie</li>
              <li>Our Marketplace Policies</li>
              <li>Merchant Dashboard</li>
              <li>Login</li>
            </ul>
          </div>
          <div>
            <h6>Need Help?</h6>
            <ul className="list-unstyled text-secondary">
              <li>FAQs</li>
              <li>Policies</li>
              <li>Contact Us</li>
            </ul>
          </div>
          <div>
            <h6>Download our App</h6>
            <ul className="list-unstyled">
              <li>
                <img
                  className="footerImageIcons"
                  src="https://ii2.pepperfry.com/media/wysiwyg/banners/web22-footer-apple-appstore-logo_2x.png"
                  alt="no-image"
                />
              </li>
              <li>
                <img
                  className="footerImageIcons mt-2"
                  src="https://ii2.pepperfry.com/media/wysiwyg/banners/web22-footer-google-playstore-logo_2x.png"
                  alt="no-image"
                />
              </li>
            </ul>
          </div>
        </div>
        <div className="d-flex justify-content-sm-between px-5 mt-4 gap-5">
          <div>
            <h6>Popular Categories</h6>
            <p className="text-secondary detailsFooterCategory">
              Sofas, Sectional Sofas, Sofa Sets, Queen Size Beds, King Size
              Beds, Coffee Tables, Dining Sets, Recliners, Sofa Cum Beds,
              Rocking Chairs, Cabinets & Sideboards, Book Shelves, TV & Media
              Units, Wardrobes, Outdoor Furniture, Bar Cabinets, Wall Shelves,
              Photo Frames, Bed Sheets, Table Linen, Study Tables, Office
              Furniture, Dining Tables, Carpets, Wall Art
            </p>
          </div>
          <div>
            <h6>Popular Brands</h6>
            <p className="text-secondary detailsFooterCategory">
              Mintwud, Woodsworth, CasaCraft, Amberville, Mudramark, Bohemiana,
              Clouddio, Spacewood, A Globia Creations, Febonic, @home, Durian,
              Evok, Hometown, Nilkamal, Crystal Furnitech, Bluewud, Duroflex,
              Sleepyhead, Green Soul, Orange Tree
            </p>
          </div>
          <div>
            <h6>Popular cities</h6>
            <p className="text-secondary">
              Bengaluru, Mumbai, Navi Mumbai, Delhi, Hyderabad, Pune, Chennai,
              Gurgaon, Kolkata, Noida, Goa, Ghaziabad, Faridabad, Jaipur,
              Lucknow, Kochi, Visakhapatnam, Chandigarh, Vadodara, Nagpur,
              Thiruvananthapuram, Indore, Mysore, Bhopal, Surat, Jalandhar,
              Patna, Ludhiana, Ahmedabad, Nashik, Madurai, Kanpur, Aurangabad
            </p>
          </div>
        </div>
        <div className="d-flex justify-content-sm-between px-5 mt-4 gap-4">
          <div>
            <h6>We accept</h6>
            <div className="d-flex gap-1">
              <img
                className="footerImageIcons"
                src="https://ii2.pepperfry.com/assets/w23-pf-visa.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-master-card.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-maestro.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-american-express.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii3.pepperfry.com/assets/w23-pf-rupay.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii3.pepperfry.com/assets/w23-pf-dinners-club.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii2.pepperfry.com/assets/w23-pf-wallet.jpg"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-net-banking.jpg"
                alt="no-image"
              />
            </div>
          </div>
          <div>
            <h6>Like What You See? Follow us Here</h6>
            <div className="d-flex gap-4">
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-social-insta.png"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii1.pepperfry.com/assets/w23-pf-social-fb.png"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii2.pepperfry.com/assets/w23-pf-social-pinterest.png"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii2.pepperfry.com/assets/w23-pf-social-linkedin.png"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii3.pepperfry.com/assets/w23-pf-social-youtube.png"
                alt="no-image"
              />
              <img
                className="footerImageIcons"
                src="https://ii2.pepperfry.com/assets/w23-pf-social-twitter.png"
                alt="no-image"
              />
            </div>
          </div>
        </div>
        <div>
          <ul className="list-unstyled d-flex justify-content-center gap-3 mt-5 text-secondary">
            <li>Whitehat</li>
            <li>Sitemap</li>
            <li>Terms Of Use</li>
            <li>Privacy Policy</li>
            <li>Your Data and Security </li>
            <li>Grievance</li>
            <li>Redressal</li>
          </ul>
        </div>
        <div className="d-flex justify-content-center text-secondary">
          © Copyright Pepperfry Limited
        </div>
      </div>
    );
  }
}

export default Footer;
