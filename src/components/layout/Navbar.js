/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import SignUpPage from "../SignUpPage";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchItem: "",
      searchStatus: false,
    };
  }

  handleChange = (event) => {
    let { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  };

  logoutFunction = () => {
    window.location.reload();
  };

  handleClick = () => {
    setTimeout(() => {
      window.location.reload();
    }, 100);
  };

  render() {
    return (
      <>
        <nav className="navbar bg-body">
          <div className="container-fluid px-5">
            <form className="d-flex" role="search">
              <input
                className="me-3 remove-border-top-right-left p-2"
                type="search"
                name="searchItem"
                placeholder="Search"
                aria-label="Search"
                onChange={(event) => {
                  return this.handleChange(event);
                }}
              />
              <Link to={`/search/${this.state.searchItem}`}>
                <button
                  className="btn"
                  type="submit"
                  onClick={this.handleClick}
                >
                  <img
                    src="https://ii1.pepperfry.com/assets/w22-search-icon.svg"
                    alt="No Image"
                  />
                </button>
              </Link>
            </form>
            <Link to="/">
              <img
                src="https://ii1.pepperfry.com/assets/w22-pf-logo.svg"
                alt="no image"
              />
            </Link>
            <div className="d-flex gap-5">
              {this.props.user === "" && (
                <div className="dropdown">
                  <button
                    className="btn dropdown"
                    type="button"
                    id="dropdownMenuButton1"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <img
                      src="https://ii1.pepperfry.com/assets/w22-profile-icon.svg"
                      alt="no image"
                    />
                  </button>
                  <ul
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenuButton1"
                  >
                    <li className="p-3 px-3 dropdownMenuButtonItem">
                      <div>
                        <h6>Welcome</h6>
                        <p>Register now and Get Exclusive Benefits !</p>
                        <button
                          type="button"
                          className="btn btn-lg loginSignUpButton"
                          data-bs-toggle="modal"
                          data-bs-target="#exampleModal"
                        >
                          LOGIN/SIGNUP
                        </button>
                      </div>
                    </li>
                  </ul>
                </div>
              )}
              {this.props.user !== "" && (
                <div className="btn-group">
                  <button type="button" className="btn">
                    Hi, {this.props.user}
                  </button>
                  <button
                    type="button"
                    className="btn dropdown-toggle dropdown-toggle-split"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <span className="visually-hidden">Toggle Dropdown</span>
                  </button>
                  <ul className="dropdown-menu">
                    <li>
                      <a className="dropdown-item" href="#">
                        Profile
                      </a>
                    </li>
                    <Link to="/customer/myorders">
                      <li>
                        <a className="dropdown-item" href="#">
                          My Orders
                        </a>
                      </li>
                    </Link>
                    <li>
                      <a className="dropdown-item" href="#">
                        My Wallet
                      </a>
                    </li>
                    <li
                      onClick={() => {
                        this.logoutFunction();
                      }}
                    >
                      <a className="dropdown-item" href="#">
                        Logout
                      </a>
                    </li>
                  </ul>
                </div>
              )}
              <img
                src="https://ii1.pepperfry.com/assets/w22-pf-notification-icon.svg"
                alt="no image"
              />
              <Link to="/Wishlist">
                <img
                  src="https://ii1.pepperfry.com/assets/w22-pf-wishlist-icon.svg"
                  alt="no image"
                  className="mt-2"
                />
                {this.props.allWishlistItems.length !== 0 && (
                  <div className="cartProductAvailableDot"></div>
                )}
              </Link>
              <Link to="/Cart">
                <img
                  src="https://ii1.pepperfry.com/assets/w22-pf-cart-icon.svg"
                  alt="no image"
                  className="mt-2"
                />
                {this.props.allCartProducts.length !== 0 && (
                  <div className="cartProductAvailableDot"></div>
                )}
              </Link>
            </div>
          </div>
        </nav>
        <div className="d-flex gap-4 justify-content-center py-3">
          <h6 className="toDoNavItem">SELL ON PEPPERFRY</h6>
          <h6 className="toDoNavItem">BECOME A FRANCHISEE</h6>
          <h6 className="toDoNavItem">BUY IN BULK</h6>
          <h6 className="toDoNavItem">FIND A STUDIO</h6>
          <h6 className="toDoNavItem">GET INSPIRED</h6>
          <h6 className="toDoNavItem">TRACK YOUR ORDER</h6>
          <h6 className="toDoNavItem">CONTACT US</h6>
        </div>
        <div className="d-flex gap-4 category-section justify-content-center text-white p-0">
          <Link to={`/Category/Category_Furnitures`}>
            <h6 className="categories pt-4 pb-3">Furniture</h6>
          </Link>
          <Link to={`/Category/Category_Mattresses`}>
            <h6 className="categories pt-4 pb-3">Matresses</h6>
          </Link>
          <Link to={`/Category/Category_HomeDecor`}>
            <h6 className="categories pt-4 pb-3">Home Decor</h6>
          </Link>
          <Link to={`/Category/Category_Furnishings`}>
            <h6 className="categories pt-4 pb-3">Furnishings</h6>
          </Link>
          <h6 className="categories pt-4 pb-3">Kitchen & Dining</h6>
          <h6 className="categories pt-4 pb-3">Lamps & Lighting</h6>
          <h6 className="categories pt-4 pb-3">Home Utility</h6>
          <h6 className="categories pt-4 pb-3">Appliances</h6>
          <h6 className="categories pt-4 pb-3">Modular</h6>
          <h6 className="categories pt-4 pb-3">Gift Cards</h6>
        </div>
        <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
          <SignUpPage />
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const user = state.signUp.usersGot;
  const allCartProducts = state.cart.productsGot;
  const allWishlistItems = state.wishlist.productsGot;
  return {
    allCartProducts,
    user,
    allWishlistItems,
  };
};

export default connect(mapStateToProps)(Navbar);
