import {
  ADD_TO_CART,
  ADD_TO_WISHLIST,
  DELETE_FROM_BUY,
  DELETE_FROM_CART,
  ORDER_THE_ITEMS,
  PURCHASE_PRODUCT,
  REMOVE_ALL_ITEMS_FROM_CART,
  REMOVE_FROM_WISHLIST,
  SIGNUPLOGIN,
  UPDATE_QUANTITY_CHANGE,
  UPDATE_QUANTITY_CHANGE_BUY,
} from "./actionTypes";

export const signUpLogin = (userGot) => {
  return {
    type: SIGNUPLOGIN,
    payload: userGot,
  };
};

export const addTOCart = (product) => {
  return {
    type: ADD_TO_CART,
    payload: product,
  };
};

export const deleteFromCart = (product) => {
  return {
    type: DELETE_FROM_CART,
    payload: product,
  };
};

export const updateQuantityChange = (product) => {
  return {
    type: UPDATE_QUANTITY_CHANGE,
    payload: [product.id, product.quantity],
  };
};

export const addToWishList = (product) => {
  return {
    type: ADD_TO_WISHLIST,
    payload: product,
  };
};

export const removeFromWishList = (product) => {
  return {
    type: REMOVE_FROM_WISHLIST,
    payload: product,
  };
};

export const orderTheItems = (orderedItems) => {
  return {
    type: ORDER_THE_ITEMS,
    payload: orderedItems,
  };
};

export const emptyCart = () => {
  return {
    type: REMOVE_ALL_ITEMS_FROM_CART,
    payload: "",
  };
};

export const purchaseProduct = (product) => {
  return {
    type: PURCHASE_PRODUCT,
    payload: product,
  };
};

export const deleteFromBuy = () => {
  return {
    type: DELETE_FROM_BUY,
    payload: "",
  };
};

export const updateQuantityChangeBuy = (product) => {
  return {
    type: UPDATE_QUANTITY_CHANGE_BUY,
    payload: [product.id, product.quantity],
  };
};
