const allProducts = require("./../assets/JSON_Files/allProducts.json");
const homepageData = require("./../assets/JSON_Files/homepageData.json")

const initialState = {
  data: allProducts.data,
  allHomepageData: homepageData,
};

const allProductsData = (state = initialState, action) => {
  switch (action.type) {
    default:
      return {
        ...state,
      };
  }
};

export default allProductsData;
