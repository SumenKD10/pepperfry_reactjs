import {
  DELETE_FROM_BUY,
  PURCHASE_PRODUCT,
  UPDATE_QUANTITY_CHANGE_BUY,
} from "./actionTypes";

const initialState = {
  productsGot: [],
};

const buy = (state = initialState, action) => {
  switch (action.type) {
    case PURCHASE_PRODUCT:
      let newProduct = action.payload;
      return {
        ...state,
        productsGot: [newProduct],
      };
    case DELETE_FROM_BUY: {
      return {
        ...state,
        productsGot: [],
      };
    }
    case UPDATE_QUANTITY_CHANGE_BUY: {
      let productsAfterUpdate = state.productsGot.map((eachProduct) => {
        const product = { ...eachProduct };
        if (Number(eachProduct.id) === Number(action.payload[0])) {
          product.quantity = Number(action.payload[1]);
          return product;
        } else {
          return eachProduct;
        }
      });
      return {
        ...state,
        productsGot: productsAfterUpdate,
      };
    }
    default:
      return {
        ...state,
      };
  }
};

export default buy;
