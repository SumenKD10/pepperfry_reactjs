import {
  ADD_TO_CART,
  DELETE_FROM_CART,
  REMOVE_ALL_ITEMS_FROM_CART,
  UPDATE_QUANTITY_CHANGE,
} from "./actionTypes";

const initialState = {
  productsGot: [],
};

const cart = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      let newProduct = action.payload;
      let productAvailable = false;
      if (state.productsGot.length !== 0) {
        let allProductIds = state.productsGot.map((eachProduct) => {
          return eachProduct.id;
        });
        if (allProductIds.includes(newProduct.id)) {
          productAvailable = true;
        }
      }
      return {
        ...state,
        productsGot:
          productAvailable === true
            ? [...state.productsGot]
            : [...state.productsGot, newProduct],
      };
    case DELETE_FROM_CART: {
      let productToDelete = action.payload;
      let newProductsAfterDelete = state.productsGot.filter((eachProduct) => {
        return eachProduct.id !== productToDelete.id;
      });
      return {
        ...state,
        productsGot: newProductsAfterDelete,
      };
    }
    case UPDATE_QUANTITY_CHANGE: {
      let productsAfterUpdate = state.productsGot.map((eachProduct) => {
        const product = { ...eachProduct };
        if (Number(eachProduct.id) === Number(action.payload[0])) {
          product.quantity = Number(action.payload[1]);
          return product;
        } else {
          return eachProduct;
        }
      });
      return {
        ...state,
        productsGot: productsAfterUpdate,
      };
    }
    case REMOVE_ALL_ITEMS_FROM_CART: {
      return {
        ...state,
        productsGot: [],
      };
    }
    default:
      return {
        ...state,
      };
  }
};

export default cart;
