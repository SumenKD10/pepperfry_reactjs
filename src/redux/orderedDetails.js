import { ORDER_THE_ITEMS } from "./actionTypes";

const initialState = {
  ordersGot: [],
};

const orderedDetails = (state = initialState, action) => {
  switch (action.type) {
    case ORDER_THE_ITEMS:
      let newOrder = action.payload;
      return {
        ...state,
        ordersGot: [newOrder, ...state.ordersGot],
      };
    default:
      return {
        ...state,
      };
  }
};

export default orderedDetails;
