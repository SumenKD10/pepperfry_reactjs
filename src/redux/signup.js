import { SIGNUPLOGIN } from "./actionTypes";

const initialState = {
  usersGot: "",
};

const signUp = (state = initialState, action) => {
  switch (action.type) {
    case SIGNUPLOGIN:
      return {
        ...state,
        usersGot: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default signUp;
