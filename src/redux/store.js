import { configureStore } from "@reduxjs/toolkit";
import signUp from "./signup";
import allProductsData from "./allProductsData";
import cart from "./cart";
import wishlist from "./wishlist";
import orderedDetails from "./orderedDetails";
import buy from "./buy";

const store = configureStore({
  reducer: {
    signUp,
    allProductsData,
    cart,
    wishlist,
    orderedDetails,
    buy,
  },
});

export default store;
