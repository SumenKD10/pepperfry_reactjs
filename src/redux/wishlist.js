import { ADD_TO_WISHLIST, REMOVE_FROM_WISHLIST } from "./actionTypes";

const initialState = {
  productsGot: [],
};

const wishlist = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_WISHLIST:
      let newProduct = action.payload;
      let productAvailable = false;
      if (state.productsGot.length !== 0) {
        let allProductIds = state.productsGot.map((eachProduct) => {
          return eachProduct.id;
        });
        if (allProductIds.includes(newProduct.id)) {
          productAvailable = true;
        }
      }
      return {
        ...state,
        productsGot:
          productAvailable === true
            ? [...state.productsGot]
            : [...state.productsGot, newProduct],
      };
    case REMOVE_FROM_WISHLIST: {
      let productToRemove = action.payload;
      let newProductsAfterRemoval = state.productsGot.filter((eachProduct) => {
        return eachProduct.id !== productToRemove.id;
      });
      return {
        ...state,
        productsGot: newProductsAfterRemoval,
      };
    }
    default:
      return {
        ...state,
      };
  }
};

export default wishlist;
